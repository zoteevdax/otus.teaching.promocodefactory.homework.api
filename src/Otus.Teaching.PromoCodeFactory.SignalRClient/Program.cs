﻿using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.SignalRClient
{
    internal class Program
    {
        const string HubUrl = "https://localhost:5001/hubs/customers";
        static async Task Main(string[] args)
        {
            // создаем подключение к хабу
            var connection = new HubConnectionBuilder()
                .WithUrl(HubUrl)
                .WithAutomaticReconnect()
                .ConfigureLogging(logging =>
                {
                    logging.SetMinimumLevel(LogLevel.Information);
                })
                .Build();

            connection.On<string>("Notify", message => Console.WriteLine(message));

            try
            {
                Console.WriteLine($"Start connection to SignalR Hub with url: {HubUrl}");
                await connection.StartAsync();

                //чтение customer
                Guid customerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
                var customer = await connection.InvokeAsync<Customer>("GetCustomer", customerId);
                Console.WriteLine($"Customer ({customerId}): {customer.FirstName} {customer.LastName} ({customer.Email})");

                //удаление customer
                await connection.InvokeAsync("DeleteCustomer", customerId);

                Console.ReadKey();
            }
            finally
            {
                await connection.StopAsync();
            }      
        }
    }
}
