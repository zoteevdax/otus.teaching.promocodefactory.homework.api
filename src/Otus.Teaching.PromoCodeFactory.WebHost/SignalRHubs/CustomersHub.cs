﻿using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.SignalRHubs
{
    public class CustomersHub: Hub
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersHub(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public async Task<List<CustomerShortResponse>> GetCustomers()
        {
            var customers = await _customerRepository.GetAllAsync();
            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return response;
        }

        public async Task<CustomerResponse> GetCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                throw new HubException($"Customer with id = [{ id }] not found");
            }

            var response = new CustomerResponse(customer);

            return response;
        }

        public async Task<Guid> CreateCustomer(CreateOrEditCustomerRequest request)
        {
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);
            await _customerRepository.AddAsync(customer);

            await Clients.All.SendAsync("Notify", $"New customer with id = {customer.Id} created");

            return customer.Id;
        }

        public async Task EditCustomers(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                throw new HubException($"Customer with id = [{id}] not found");
            }

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            CustomerMapper.MapFromModel(request, preferences, customer);
            await _customerRepository.UpdateAsync(customer);

            await Clients.All.SendAsync("Notify", $"New customer with id = {customer.Id} updated");
        }

        public async Task DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                throw new HubException($"Customer with id = [{id}] not found");
            }

            await _customerRepository.DeleteAsync(customer);

            await Clients.All.SendAsync("Notify", $"Customer with id = {customer.Id} deleted");
        }

    }
}
