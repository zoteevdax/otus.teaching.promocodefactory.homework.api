﻿using Castle.Core.Resource;
using Google.Protobuf.Collections;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.Grpc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GrpcServices
{
    public class CustomersService: Customers.CustomersBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersService(IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<CustomerShortResponseList> GetCustomers(Empty request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return new CustomerShortResponseList { Customers = { response } };
        }

        public override async Task<CustomerResponse> GetCustomer(CustomerId request, ServerCallContext context)
        {
            var customerId = GetValidCustomerGuid(request.Id);
            var customer = await _customerRepository.GetByIdAsync(customerId) ?? 
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found"));

            var response = new CustomerResponse
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
            };

            foreach (var pref in customer.Preferences)
            {
                response.Preferences.Add(new PreferenceResponse
                {
                    Id = pref.PreferenceId.ToString(),
                    Name = pref.Preference.Name
                });
            }

            return response;
        }

        public override async Task<CustomerId> CreateCustomer(CreateCustomerRequest request, ServerCallContext context)
        {
            var preferencesGuids = GetValidPreferencesGuids(request.PreferenceIds);
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(preferencesGuids);

            Customer customer = MapToCustomer(request, preferences);

            await _customerRepository.AddAsync(customer);

            return new CustomerId { Id = customer.Id.ToString() };
        }

        public override async Task<Empty> EditCustomers(EditCustomerRequest request, ServerCallContext context)
        {
            var customerId = GetValidCustomerGuid(request.Id);
            var customer = await _customerRepository.GetByIdAsync(customerId);

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found"));

            var preferencesGuids = GetValidPreferencesGuids(request.PreferenceIds);
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(preferencesGuids);

            UpdateCustomer(customer, request, preferences);

            await _customerRepository.UpdateAsync(customer);

            return new Empty();
        }

        public override async Task<Empty> DeleteCustomer(CustomerId request, ServerCallContext context)
        {
            var customerId = GetValidCustomerGuid(request.Id);
            var customer = await _customerRepository.GetByIdAsync(customerId);

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found"));

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }

        private Customer MapToCustomer(CreateCustomerRequest request, IEnumerable<Preference> preferences)
        {
            var customer = new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences.Select(x => new CustomerPreference()
                {
                    Preference = x,
                    PreferenceId = x.Id
                }).ToList()
            };

            customer.Id = Guid.NewGuid();

            return customer;
        }

        private void UpdateCustomer(Customer customer, EditCustomerRequest request, IEnumerable<Preference> preferences)
        {
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();
        }

        private Guid GetValidCustomerGuid(string id)
        {
            if (!Guid.TryParse(id, out var customerGuid))
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Incorrect customer id"));

            return customerGuid;
        }

        private List<Guid> GetValidPreferencesGuids(IEnumerable<string> preferencesIds)
        {
            var preferencesGuids = new List<Guid>();
            foreach (var preferenceId in preferencesIds)
            {
                if (!Guid.TryParse(preferenceId, out Guid guid))
                    throw new RpcException(new Status(StatusCode.InvalidArgument, "Incorrect preference id"));

                preferencesGuids.Add(guid);
            }

            return preferencesGuids;
        }
    }
}
