﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Net.Client;
using Otus.Teaching.PromoCodeFactory.Grpc;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GrpcClient
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Connecting to GRPC Server and request customers list...");

            using var channel = GrpcChannel.ForAddress("https://localhost:5001/");
            var customerClient = new Customers.CustomersClient(channel);


            var response = await customerClient.GetCustomersAsync(new Empty(), deadline: DateTime.UtcNow.AddSeconds(30));
            foreach (var customer in response.Customers)
                Console.WriteLine($"Customer ({customer.Id}): {customer.FirstName} {customer.LastName} ({customer.Email})");


            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
